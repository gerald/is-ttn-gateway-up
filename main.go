package main

import (
	"bytes"
	"encoding/json"
	"flag"
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
	"path"
	"strconv"
	"strings"
	"time"
)

const apiURLTemplate = "https://eu1.cloud.thethings.network/api/v3/gs/gateways/%s/connection/stats"

type IntFromString int

type APIResponse struct {
	ConnectedAt            time.Time     `json:"connected_at"`
	LastUplinkReceivedAt   time.Time     `json:"last_uplink_received_at"`
	LastDownlinkReceivedAt time.Time     `json:"last_downlink_received_at"`
	UplinkCount            IntFromString `json:"uplink_count"`
	DownlinkCount          IntFromString `json:"downlink_count"`
}

func (i *IntFromString) UnmarshalJSON(b []byte) error {
	var s string
	if err := json.Unmarshal(b, &s); err != nil {
		return err
	}

	intVal, err := strconv.Atoi(s)
	if err != nil {
		return err
	}

	*i = IntFromString(intVal)

	return nil
}

func (i IntFromString) MarshalJSON() ([]byte, error) {
	return json.Marshal(strconv.Itoa(int(i)))
}

// Compare a given APIResponse to the current one
// Current should be the older one
func (a *APIResponse) Compare(b *APIResponse) error {
	// Check if the Uplink/Downlink timestamps of b are not the same as timestamps of a
	if a.LastUplinkReceivedAt.Equal(b.LastUplinkReceivedAt) && a.LastDownlinkReceivedAt.Equal(b.LastDownlinkReceivedAt) {
		diffUp := time.Now().Sub(a.LastUplinkReceivedAt)
		diffDown := time.Now().Sub(a.LastDownlinkReceivedAt)
		return fmt.Errorf("uplink and downlink timestamps are equal (up: %v, down: %v). %s and %s from now", a.LastUplinkReceivedAt, a.LastDownlinkReceivedAt, diffUp, diffDown)
	}

	// Check if the uplink/downlink timestamps of b are later than timestamps of a
	if a.LastUplinkReceivedAt.After(b.LastUplinkReceivedAt) || a.LastDownlinkReceivedAt.After(b.LastDownlinkReceivedAt) {
		return fmt.Errorf("time travel detected")
	}

	// Check `connected_at` value before checking TX/RX values
	// as they're not persisted between reconnects of the gateway
	if a.ConnectedAt.Before(b.ConnectedAt) {
		return nil
	}

	// Check if we have received some messages
	if b.UplinkCount-a.UplinkCount < 0 {
		return fmt.Errorf("received a negative amount of messages (%d)", b.UplinkCount-a.UplinkCount)
	}

	// Check if we have sent some messages
	if b.DownlinkCount-a.DownlinkCount < 0 {
		return fmt.Errorf("sent a negative amount of messages (%d)", b.DownlinkCount-a.DownlinkCount)
	}

	return nil
}

func (a *APIResponse) Save(filepath string) error {
	datasetBytes, err := json.Marshal(a)
	if err != nil {
		return err
	}

	return os.WriteFile(filepath, datasetBytes, 0644)
}

var (
	flagGatewayID, flagComparisonFilePath string
	envAPIToken                           string
)

func bytesToResponse(bytes []byte) (*APIResponse, error) {
	var ttnResp APIResponse
	err := json.Unmarshal(bytes, &ttnResp)
	if err != nil {
		return nil, err
	}

	return &ttnResp, nil
}

func readPreviousResponseFromDisk(filepath string) (*APIResponse, error) {
	bytes, err := os.ReadFile(filepath)
	if err != nil {
		return nil, err
	}

	return bytesToResponse(bytes)
}

func getUptimeResponse() (*APIResponse, error) {
	url := fmt.Sprintf(apiURLTemplate, flagGatewayID)
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return nil, fmt.Errorf("Error constructing request for url '%s': %v", url, err)
	}
	req.Header.Set("Authorization", "Bearer "+envAPIToken)

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return nil, fmt.Errorf("Error fetching url '%s': %v", url, err)
	}
	defer resp.Body.Close()

	respBytes, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("TTN responded with status code %d (%s)", resp.StatusCode, bytes.TrimSpace(respBytes))
	}

	return bytesToResponse(respBytes)
}

func makeDefaultPath() string {
	filename := fmt.Sprintf("status-%s.json", flagGatewayID)
	filepath := "./" + filename

	dir, err := os.Getwd()
	if err == nil {
		filepath = path.Join(dir, filename)
	}

	return filepath
}

func main() {
	flag.StringVar(&flagGatewayID, "id", "", "gateway id (not eui)")
	flag.StringVar(&flagComparisonFilePath, "f", "", `path to comparison file. will be created if nonexistent. (default "./data-<value of -id flag>.json")`)
	flag.Parse()
	if flagGatewayID == "" {
		log.Fatalln("flag -id is required")
	}
	envAPIToken = os.Getenv("TTN_API_TOKEN")
	if envAPIToken == "" {
		log.Fatalln("environment variable TTN_API_TOKEN is required")
	}
	if flagComparisonFilePath == "" {
		flagComparisonFilePath = makeDefaultPath()
	}

	fileResponse, err := readPreviousResponseFromDisk(flagComparisonFilePath)
	if os.IsNotExist(err) {
		fmt.Printf("File %s not found. Going to create one from %s%s\n", flagComparisonFilePath, apiURLTemplate, flagGatewayID)

		apiResponse, err := getUptimeResponse()
		if err != nil {
			log.Fatalf("Error requesting status from network: %v\n", err)
		}
		err = apiResponse.Save(flagComparisonFilePath)
		if err != nil {
			log.Fatalf("Saving of network response failed: %v\n", err)
		}

		fmt.Printf("File %s created.\n\nRe-run %s for normal comparison\nRunning this immediatley will likely fail as the file and api response are still the same\n", flagComparisonFilePath, strings.Join(os.Args, " "))

		return
	} else if err != nil {
		log.Fatalf("Error reading status from disk: %v\n", err)
	}

	apiResponse, err := getUptimeResponse()
	if err != nil {
		log.Fatalf("Error requesting status from network: %v\n", err)
	}

	err = fileResponse.Compare(apiResponse)
	if err != nil {
		log.Fatalf("Comparison of responses failed: %v\n", err)
	}

	err = apiResponse.Save(flagComparisonFilePath)
	if err != nil {
		log.Fatalf("Saving of network response failed: %v\n", err)
	}
}
