# is-ttn-gateway-up

```
Usage of is-ttn-gateway-up:
  -id string
      gateway id (not eui)
  -f string
      path to comparison file. will be created if nonexistent. (default "./data-<value of -id flag>.json")
```

It is also required to set an environment variable named `TTN_API_TOKEN` containing a valid TTN v3 API token. The token requires the 'View Gateway status' permission.

## Bootstrap

The first time running this, there will be no reference for comparing API responses.

You can create a reference file by simply executing `is-ttn-gateway-up`. It'll fetch an API response and store the result on disk.

## License

MIT
